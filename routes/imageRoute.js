var express = require('express');
var router = express.Router();
var multer = require('multer');
var User = require('../models/user');

// router.getImages = function(callback, limit) {
//     Image.find(callback).limit(limit);
// }
//
// router.getImageById = function(id, callback) {
//     Image.findById(id, callback);
// }

router.addImage = function(image, callback) {
    // User.create(image, callback);
    console.log(image);
}

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'app/ProfileImages')
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname);
    }
});

var upload = multer({
    storage: storage
});

router.post('/photo', upload.any(), function(req, res, next) {

    res.send(req.files);
    var path ='ProfileImages/'+req.files[0].originalname;
    var imagepath = {};
    imagepath['path'] = path;
    imagepath['id'] = req.user._id;

    User.update({_id: req.user._id}, {
        image: path
    }, function(err) {
        if(err) throw err;
    })
});

module.exports = router;