var config = require('config.js');
var express = require('express');
var router = express.Router();
var User = require('../controllers/userController');

/** USER`s routes **/
router.get('/', User.getAll);
router.get('/rooms', User.getRooms);
router.get('/me', User.me);
router.post('/addNewUser', User.add);
router.delete('/:_id', User.delete);

/** AUTH routes **/
router.post('/register', User.add);
router.post('/login',User.auth);

module.exports = router;





