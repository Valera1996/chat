// require('rootpath')();
// var express = require('express');
// var app = express();
// var http = require('http').Server(app);
// var io = require('socket.io')(http);
// var port = process.env.PORT || 3000;
// var session = require('express-session');
// var bodyParser = require('body-parser');
// var expressJwt = require('express-jwt');
// var config = require('./config');
// var mongoose = require('mongoose');
// var multer = require('multer');
//
// app.use(express.static(__dirname + '/app'));
//
// app.use(bodyParser.urlencoded({extended: false}));
// app.use(bodyParser.json());
//
// /** use JWT auth to secure the app **/
// app.use(expressJwt({secret: config.secret, credentialsRequired: false}).unless({path: ['/api/users/login']}));
//
// /** ROUTES **/
// app.use('/api/users', require('./routes/userRoute.js'));
// require('./controllers/chatController.js')(io);
//
// /** CONNECT TO DB **/
// mongoose.connect(config.connectionString, function (err) {
//     if (err) {
//         console.log(err);
//     } else
//     {
//         console.log('Connected to mongodb');
//     };
// });
//
// http.listen(port, function() {
//   console.log('listening on *:' + port);
// });


require('rootpath')();
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = process.env.PORT || 3000;
var session = require('express-session');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('./config');
var mongoose = require('mongoose');
var multer = require('multer');
var path = require('path');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + '/app'));

/** use JWT auth to secure the app **/
app.use(expressJwt({secret: config.secret, credentialsRequired: false}).unless({path: ['/api/users/login']}));

/** ROUTES **/
app.use('/api/users', require('./routes/userRoute.js'));
app.use('/', require('./routes/imageRoute.js'));
require('./controllers/chatController.js')(io);

mongoose.connect(config.connectionString, function (err) {
    if (err) {
        console.log(err);
    } else
    {
        console.log('Connected to mongodb');
    };
});

http.listen(port, function() {
  console.log('listening on *:' + port);
});