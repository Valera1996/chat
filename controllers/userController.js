var config = require('config.js');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var Users = require('../models/user');
var Rooms = require('../models/room');

/** GET AUTH USER **/
module.exports.me = function (req, res) {
    res.send(req.user);
};

/** GET ALL USERS **/
module.exports.getAll=function (req, res) {
    // if (!req.user) return res.sendStatus(401);
    var page = +req.query['page'];
    var limit = +req.query['limit'];
    Users.paginate({}, {page: page, limit: limit}, function (err, data) {

        var users = [{
            username: 'admin',
            nick: 'admin',
            password: 'admin',
            role: 'Администратор',
            image:"images/default.png"
        }, {
            username: 'user',
            nick: 'user',
            password: 'user',
            role: 'Пользователь',
            image:"images/default.png"
        } ];
        if (!data.total>0){
            for (var c = 0; c < users.length; c++) {
                var user = _.omit(users[c], 'password');
                var newUser = new Users(users[c]);
                newUser.hash = bcrypt.hashSync(users[c].password, 10);
                newUser.save(function(err, savedChat) {
                });
            }
        }
        res.send(data);
    });
};

/** GET ALL ROOMS **/
module.exports.getRooms=function (req, res) {
    Rooms.paginate({},{},  function (err, data) {
        res.send(data);
    });
};

/** ADD NEW USER **/
module.exports.add = function (req) {
    var userParam=req.body;
    var deferred = Q.defer();

/** VALIDATION **/
    Users.findOne(
        {username: userParam.username},
        function (err, user) {
            if (err)
                deferred.reject(err);
            if (user) {
                deferred.reject('Username "' + userParam.username + '" is already taken');
            } else {
                createUser();
            }
        });
    function createUser() {
        var user = _.omit(userParam, 'password');
        user.hash = bcrypt.hashSync(userParam.password, 10);
        Users.create(
            user,
            function (err, doc) {
                if (err) deferred.reject(err);

                deferred.resolve();
            });
    };
    return deferred.promise;
};

/** DELETE USER **/
module.exports.delete = function (req, res) {
    var id=req.params._id;
    Users.findOne({ _id: id}, function (err, user) {
        if(user!==null)
            user.remove(function (err) {});
        res.send(user);
    });
};

/** AUTH NEW USER **/
module.exports.auth = function (req, res) {
    var body = req.body;
    auth (body.username, body.password)
        .then(function (token) {
            if (token) {
                // authentication successful
                res.send({token: token, user: body});
            } else {
                // authentication failed
                res.status(401).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
};

/** AUTH FUNCTION **/
function auth (username, password) {
    var deferred = Q.defer();
    Users.findOne({username: username}, function (err, user) {
        if (err) deferred.reject(err);
        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            deferred.resolve(jwt.sign({
                _id: user._id,
                username: user.username,
                role: user.role,
                nick: user.nick,
                info: user.info,
                photo: user.image
            }, config.secret));
        } else {
            // authentication failed
            deferred.resolve();
        }
    });
    return deferred.promise;
};

