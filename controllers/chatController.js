var mongoose = require('mongoose');
var Chat = require('../models/chat');
var Room = require('../models/room');

module.exports = function(io) {

    var users = [];
    var connections = [];

    /** DEFAULT ROOMS **/
    var rooms = [
        {id: mongoose.Types.ObjectId(), room:'Global', usersOnline:[]},
        {id: mongoose.Types.ObjectId(), room:'room1', usersOnline:[]},
        {id: mongoose.Types.ObjectId(), room:'room2', usersOnline:[]},
        {id: mongoose.Types.ObjectId(), room:'room3', usersOnline:[]}
    ];

    Room.find({}).exec(function (err, docs) {
        if (err) throw err;
        if (!docs.length > 0) {
            for (var i = 0; i < rooms.length; i++) {
                var NewRooms = new Room({_id: rooms[i].id,room: rooms[i].room});
                NewRooms.save(function (err, docs) {
                    if (err) throw err;
                })
            }
        }
        else {
            rooms=[];
            var obj = {};
            for (var i = 0; i < docs.length; i++) {
                obj.id=docs[i]._id;
                obj.room=docs[i].room;
                obj.usersOnline=[];
                rooms.push(obj);
                obj = {};
            }
        }
    })

    io.sockets.on('connection', function(socket) {

        connections.push(socket);
        console.log('Socket on connection');

        socket.room = rooms[0].room;
        socket.roomID=rooms[0].id;

        updateRooms();

        var query = Chat.find({roomID: socket.roomID  });
        query.sort('-date').limit(10).exec(function (err, docs) {
            if(err) throw err;
            socket.emit('load old msg', docs);
        });

        /** ROOMS **/
        socket.on('new room', function(room) {
            var obj={};
            obj.id=mongoose.Types.ObjectId();
            obj.room=room;
            rooms.push(room);
            rooms.push(obj);
            var NewRoom = new Room({room: room, _id: obj.id});
            NewRoom.save(function (err, docs) {
                if (err) throw err;
            })
            updateRooms();
        });

        function findiD(rooms) {
            return rooms.room === socket.room;
        }

        socket.on('switchRoom', function(newroom) {

            if(socket.room == newroom)
                socket.emit('new message', {msg: 'you already in this room.', nick: 'SERVER'});
            else
            {
                var index = rooms.find(findiD).usersOnline.indexOf(socket.username);
                rooms.find(findiD).usersOnline.splice(index, 1);
                socket.leave(socket.room);
                socket.join(newroom);
                socket.emit('new message', {msg: 'you have connected to ' + newroom, nick: 'SERVER'});

                socket.broadcast.to(socket.room).emit('new message', {
                    msg: socket.username + ' has left this room',
                    nick: 'SERVER'
                });
                socket.room = newroom;
                socket.roomID=rooms.find(findiD).id;
                rooms.find(findiD).usersOnline.push(socket.username);

                socket.broadcast.to(newroom).emit('new message', {
                    msg: socket.username + ' has joined this room',
                    nick: 'SERVER'
                });

                var query = Chat.find({roomID: socket.roomID });
                query.sort('-date').limit(10).exec(function (err, docs) {
                    if(err) throw err;
                    if(!docs.length>0){
                        docs    = [ {
                            nick:'SERVER',
                            msg:'Message history is empty'
                        }]
                        socket.emit('load old msg', docs);
                    }
                    else
                        socket.emit('load old msg', docs);
                });
                updateRooms(rooms);
            }
        });

        function updateRooms() {
            var obj = {
                rooms:rooms,
                current:socket.room
            };
            io.sockets.emit('get rooms', obj);
        };

        /** USERS **/
        function updateUsernames() {
            io.sockets.emit('get users', users)
        };

        socket.on('new user', function(username,callback) {
            callback(true);
            console.log('connection: ',username);
            socket.username = username;
            users.push(socket.username);
            updateUsernames();
            socket.join(socket.room);
            socket.emit('new message',{msg: 'you have connected to GLOBAL', nick: 'SERVER'});
            socket.broadcast.to(socket.room).emit('new message',{msg: username + ' has connected to this room', nick: 'SERVER'} );
            socket.roomID=rooms.find(findiD).id;
            rooms.find(findiD).usersOnline.push(socket.username);
            updateRooms();
        });

        /** MESSAGES **/
        socket.on('send message', function (data) {
            console.log(data);
            var NewMsg = new Chat({msg: data.msg, nick: socket.username, room: socket.room, roomID: socket.roomID, userID: data.id, photo: data.photo});
            NewMsg.save(function (err) {
                if(err) throw err;
                io.sockets.in(socket.room).emit('new message',{ msg: data.msg, nick: socket.username, userID: data.id, photo: data.photo });
            })
        });

        /** DISCONNECTION **/
        socket.on('disconnect', function() {
            if (!(socket.username == undefined)) {
                console.log('disconnection: ',socket.username);
                users.splice(users.indexOf(socket.username), 1);
                var index = rooms.find(findiD).usersOnline.indexOf(socket.username);
                rooms.find(findiD).usersOnline.splice(index, 1);
                updateUsernames();
                connections.splice(connections.indexOf(socket), 1);
                var Server = 'Server';
                socket.broadcast.emit('notice', {msg: socket.username + ' has left the chat.', nick: Server});
                socket.leave(socket.room);
            }
        });

        socket.on('logout', function(username) {
            users.splice(users.indexOf(username),1);
            updateUsernames();
            connections.splice(connections.indexOf(socket), 1);
            var index = rooms.find(findiD).usersOnline.indexOf(socket.username);
            rooms.find(findiD).usersOnline.splice(index, 1);
            io.sockets.emit('updateusers', users);
            socket.broadcast.emit('notice',{ msg: socket.username + ' has left the chat.',nick: 'Server'});
            socket.leave(socket.room);
        });

        socket.on('ban', function(id) {
            io.sockets.emit('ban',id);
        });

    });


};