var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

var Users = Schema({
    username: String,
    nick:String,
    image:String,
    hash: String,
    role: String
});

Users.plugin(mongoosePaginate);
var Users = module.exports = mongoose.model('Users', Users);

