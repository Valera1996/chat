var mongoose = require('mongoose');

var chatSchema = mongoose.Schema({
    nick:   String,
    msg:    String,
    room:   String,
    photo:  String,
    userID:  mongoose.Schema.Types.ObjectId,
    roomID:  mongoose.Schema.Types.ObjectId,
    date:  {type:String, default:Date }
});

var Chat = module.exports = mongoose.model('Message',chatSchema);