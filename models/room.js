var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var roomSchema = mongoose.Schema({
    room:   String
});

roomSchema.plugin(mongoosePaginate);
var Room = module.exports = mongoose.model('Room',roomSchema);