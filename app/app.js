﻿(function () {
    'use strict';

    angular
        .module('app', ['ui.router', 'ui.bootstrap'])
        .config(config)
        .run(run);

    function config($stateProvider, $urlRouterProvider, $httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
        // default route
        $urlRouterProvider.otherwise("/");

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'views/global.html',
                controller: 'chatController',
                controllerAs: 'chatCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'views/login.html',
                controller: 'authController',
                controllerAs: 'authCtrl'
            })

    };

    function run($rootScope, $state, userFactory) {

    };

    $(function () {
        angular.bootstrap(document, ['app']);
    });

})();

