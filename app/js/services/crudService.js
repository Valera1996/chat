(function () {
    'use strict';

    angular.module('app').factory('crudService', crudService);

    function crudService($http, $q) {

        var service = {};

        service.getAll = getAll;
        service.Create = Create;
        service.Authenticate = Authenticate;
        service.addNewUser = addNewUser;
        service.getRooms = getRooms;
        service.addImage = addImage;

        return service;

        function getAll(page, limit) {
            return $http.get('/api/users', {
                params: {
                    page: page,
                    limit: limit
                }
            });
        };

        function getRooms() {
            return $http.get('/api/users/rooms');
        };

        function Create(user) {
            return $http.post('/api/users/register', user).then(handleSuccess, handleError);
        };
        function addImage (data) {
            return $http.post('/api/users/image', data).then(handleSuccess, handleError);
        };

        function addNewUser(user) {
            return $http.post('/api/users/addNewUser', user).then(handleSuccess, handleError);
        };

        function Authenticate(user) {
            return $http.post('/login', user).then(handleSuccess, handleError);
        };

        // private functions
        function handleSuccess(res) {
            return res.data;
        };

        function handleError(res) {
            return $q.reject(res.data);
        };

    };

})();