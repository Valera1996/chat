﻿(function () {
    'use strict';
    angular.module('app').controller('chatController', Controller);

    function Controller(userFactory,  $scope, crudService, $http, $state, $uibModal) {
        console.log("chatController loaded");

        var socket = io.connect();

        $scope.rooms = [];
        $scope.room = 'Global';
        $scope.messages = {};
        $scope.onlineUsers = [];
        $scope.room=$scope.rooms[0];
        $scope.users = [];
        $scope.nick=null;
        $scope.showMe=true;
        $scope.myID=null;
        $scope.myPhoto=null;

        $scope.login = function (username, password) {

            userFactory.login(username, password).then(function success(response) {
                $scope.user = response.data.user;
                $scope.username=$scope.user.username;
                socket.emit('new user', $scope.user.username, function(data){
                });

                userFactory.getUser().then(function success(response) {
                    $scope.myID=response.data._id;
                    $scope.myPhoto=response.data.photo;
                    var auth = response.data.role;
                    $scope.nick = response.data.nick;
                    if (auth === 'Администратор')   $scope.myValue=false;
                    else $scope.myValue=true;

                    $scope.showMe=false;
                });
            }, $scope.handleError);

        };

            // SEND MESSAGE
            $scope.sendMessage = function (msg) {

                $scope.data={
                    id: $scope.myID,
                    photo: $scope.myPhoto,
                    msg:msg
                };
                socket.emit('send message', $scope.data  );
                $scope.message=null;
            };

            socket.on('new message', function(data){
                console.log('new mes',data);
                if(!data.photo)
                {
                    data.photo='images/default.png';
                }
                $scope.createMessage(data);
                $scope.message = "";
            });

            $scope.createMessage = function(data) {
                console.log('create mes',data);
                if(data.userID == $scope.myID)
                {
                    data.myMessage=true;
                }
                else{
                    data.myMessage=false;
                }

                $scope.messages.unshift(data);
                $scope.$apply();
            };

            socket.on('notice', function(msg) {
                $scope.createMessage(msg);
            });

            socket.on('load old msg', function(data) {

                $scope.messages = [];
                for (var i =data.length-1; i >= 0 ; i--) {
                    if(!data[i].photo){
                        data[i].photo='images/default.png';
                    }
                    $scope.createMessage(data[i]);
                }
            });

            // PRESSING 'ENTER' HANDLER
            $scope.myFunct = function(keyEvent, message) {
                if (keyEvent.which === 13) {
                    $scope.sendMessage(message);
                }
            };

            // GET USERS
            socket.on('get users', function(data){
                $scope.users=[];
                for (var i = 0; i < data.length; i++) {
                    $scope.users.push(data[i]);
                    $scope.$apply();
                }
            });

            userFactory.getUser().then(function success(response) {
                $scope.myID=response.data._id;
                $scope.myPhoto=response.data.photo;
                $scope.username=response.data.username;
                $scope.nick = response.data.nick;
                var auth = response.data.role;
                if (auth === 'Администратор')   $scope.myValue=false;
                else $scope.myValue=true;
                socket.emit('new user', $scope.username,function(data){
                    if(data){
                  $scope.showMe=false;
                    }
                });
            });

            // LOGIN
            $scope.changeState = function () {
                $state.go('login');
                socket.disconnect();
            };

            // ROOMS
            $scope.switchRoom = function (room){
                $scope.room=room;
                socket.emit('switchRoom', room);
            };

            socket.on('get rooms', function(data){
                if($scope.room)
                {
                    var findField =$scope.room;
                }
                else
                {
                    var findField =data.current;
                }

                function findId(rooms) {
                    return rooms.room === findField;
                }

                $scope.onlineUsers= data.rooms.find(findId).usersOnline;

                $scope.rooms=[];
                    for (var i = 0; i < data.rooms.length; i++) {
                        $scope.rooms.push(data.rooms[i].room);
                        $scope.$apply();
                    }
            });

            // DISCONNECTION
            $scope.logout = function () {
                userFactory.getUser().then(function then(response) {
                    userFactory.logout();
                    $scope.showMe=true;
                    $scope.username=response.data.username;
                    socket.emit('logout',  $scope.username );
                    $scope.users.splice($scope.users.indexOf( $scope.username),1);
                });
            };

            socket.on('ban', function(id) {
                    userFactory.getUser().then(function then(response) {
                        $scope.username=response.data.username;
                        if(id == $scope.username )
                        {
                            socket.disconnect();
                            $scope.showMe=true;
                            userFactory.logout();
                            $scope.users.splice($scope.users.indexOf( $scope.username),1);
                        }
                    });
                });

            $scope.ban = function (id) {
                socket.emit('ban', id );
            };

            // ADD ROOM MODAL
            $scope.openAskModal = function(){
                $scope.modalInstance = $uibModal.open({
                    templateUrl: 'courseForm.tmpl.html',
                    scope: $scope
                });
            };

            $scope.closeAskModal = function(){
                $scope.modalInstance.dismiss();
            };

            $scope.askQuestion = function(room) {
                $scope.modalInstance.dismiss();
                socket.emit('new room',room );
            };

            $scope.openInfoModal = function(){
                $scope.modalInstance = $uibModal.open({
                    templateUrl: 'info.tmpl.html',
                    scope: $scope
                });
            };
            $scope.closeInfoModal = function(){
                $scope.modalInstance.dismiss();
            };

            $scope.submitInfoModal = function() {
                $scope.modalInstance.dismiss();
            };

        $scope.uploadFile = function(files) {
            var fd = new FormData();
            //Take the first selected file
            fd.append("file", files[0]);

            $http.post('/photo', fd,  {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).success( function (response) {
            });
        };

            // ERROR HANDLER
            $scope.handleError = function (response) {
               console.log('Error: ' + response);
            };

            crudService.getAll($scope.currentPage, 3).success(function (response) {
              $scope.totalItems = response.total;
                 });


    };
})();