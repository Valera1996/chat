app.controller('authController', function authController(userFactory, crudService, $scope,$state) {
    console.log("authController loaded");
    'use strict';
    $scope.worker = null;

    crudService.getAll($scope.currentPage, 3).success(function (response) {
        $scope.totalItems = response.total;
        $scope.user = {};
        if ($scope.totalItems) {
            $scope.user.role = 'Руководитель';
        }
        else {
            $scope.user.role = 'Администратор';
        }
    });

    $scope.reg = function () {
        userFactory.reg($scope.user).then(function success(response) {
        }, handleError);
        $scope.backToGlobal();
    };

    $scope.backToGlobal = function () {
        $state.go('home');
    };

    $scope.logout = function () {
        userFactory.logout();
        $scope.user = null;
    };

    function handleError(response) {
        alert('Error: ' + response);
    };

});
